# Author: Robson de Sousa

# funcão que lê todas as linhas do arquivo e adiciona cada linha a uma lista vazia


def convert_file_to_matrix(file_name):
    return [line.strip().split() for line in open(file_name, 'r').readlines()]


'''
def convert_file_to_list(file):

    damas = []
    queridos = {}
    arq = open(file, "r")

    for line in arq:
        line = line.strip().split()
        queridos[line[0]] = line[1:]
        damas.append(line[0])
    arq.close()

    return queridos
'''

# pythonic mode
'''
def convert_file_to_list(file):
    itens = []
    with open(file, 'r') as arq:
        for line in arq:
            itens.append(line.strip().split())
    return itens
'''

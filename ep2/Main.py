from ep2 import File
from ep2 import Merlin

# Estudar as funções builtins.py e dicionários

# Author: Robson de Sousa

casamento_txt = File.convert_file_to_matrix('casamento.txt')
casamento_no_txt = File.convert_file_to_matrix('casamento no.txt')


def testa_casamento(matrix):
    damas = []
    queridos = {}

    for i, k in enumerate(matrix):
        # for x in k:
        queridos[k[0]] = k[1:]
        damas.append([k][0][0])
        print("Queridos: ", queridos)
        print("Damas: %s" % damas)

    damas_enumeradas = Merlin.enumeracoes(damas)

    for enumerada in damas_enumeradas:
        lista = []
        for dama in enumerada:
            lista.extend(queridos[dama])

        if len(enumerada) > len(set(lista)):
            print("Não é possível casar todas")
            enumerada = ' e '.join(enumerada)
            lista = ' '.join(set(lista))
            print(f'{enumerada} gostam de {lista}')

            break
    else:
        print("Possível casar todas")
    print()

    print("\n####################################################################\n")


cavaleiros_txt = File.convert_file_to_matrix('cavaleiros.txt')
cavaleiros_no_txt = File.convert_file_to_matrix('cavaleiros no.txt')


def testa_cavaleiros(matrix):
    amigos = {}
    cavaleiros = []

    for i, k in enumerate(matrix):
        # for x in k:
        amigos[k[0]] = k[1:]
        cavaleiros.append([k][0][0])
        print("Cavaleiros: ", cavaleiros)
        print("Amigos: %s" % amigos)
    print()

    cavaleiros_permutados = Merlin.permutacoes(cavaleiros)

    for cavaleiro in cavaleiros_permutados:
        for k in range(len(cavaleiro)):
            if cavaleiro[k] not in amigos[cavaleiro[(k + 1) % len(cavaleiro)]]:
                break
        else:
            print("Conseguimos uma mesa")
            print(' '.join(cavaleiro))
            break
    else:
        print("Não conseguimos um lugar a mesa")

    print("\n####################################################################\n")


if __name__ == '__main__':
    print()
    testa_casamento(casamento_txt)
    testa_casamento(casamento_no_txt)
    testa_cavaleiros(cavaleiros_txt)
    testa_cavaleiros(cavaleiros_no_txt)

'''
from random import sample

fat = sample(range(50), 8)


def factorial(number):
    if number <= 1:
        return 1
    else:
        return number * factorial(number - 1)


# print(factorial(7))

print(fat)
'''


def bem_formada(s):
    p = []
    for c in s:
        if c == ')':
            if p[-1] == '(':
                p.pop()
            else:
                return False
        elif c in ']':
            if p[-1] == '[':
                p.pop()
            else:
                return False
        elif c == '}':
            if p[-1] == '{':
                p.pop()
            else:
                return False
        elif c == '>':
            if p[-1] == '<':
                p.pop()
            else:
                return False
        else:
            p.append(c)
    return len(p) == 0


print(bem_formada('{([])}'))
print(bem_formada('((){()})'))
print(bem_formada('({)}'))
print(bem_formada('<[([])]>'))
print(bem_formada('()[]{()}'))

